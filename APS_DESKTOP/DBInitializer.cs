﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using APS_CORE;

namespace APS_DESKTOP
{
    public class DBInitializer
    {
        AppDBEntities db = new AppDBEntities();
        public static void initialize()
        {
            DBInitializer dbinit = new DBInitializer();
            dbinit.initializeMarca();
            dbinit.initizalizeModelo();
            dbinit.initializeItemFipe();
        }

        protected void initializeMarca()
        {
            if (db.Marca.ToList().Count == 0)
            {
                db.Marca.Add(new Marca()
                {
                    id = 1,
                    marca1 = "VW - Volkswagen"
                });
                db.Marca.Add(new Marca()
                {
                    id = 2,
                    marca1 = "Fiat"
                });
                db.SaveChanges();
            }
        }

        protected void initizalizeModelo()
        {
            if (db.Modelo.ToList().Count == 0)
            {
                db.Modelo.Add(new Modelo()
                {
                    id = 1,
                    id_marca = 1,
                    modelo1 = "Gol"
                });
                db.Modelo.Add(new Modelo()
                {
                    id = 2,
                    id_marca = 2,
                    modelo1 = "Uno"
                });
                db.SaveChanges();
            }
        }

        protected void initializeItemFipe()
        {
            if (db.ItemFipe.ToList().Count == 0)
            {
                db.ItemFipe.Add(new ItemFipe()
                {
                    id = 1,
                    id_modelo = 1,
                    ano = 2003,
                    valor = 11500.00
                });
                db.ItemFipe.Add(new ItemFipe()
                {
                    id = 2,
                    id_modelo = 2,
                    ano = 2005,
                    valor = 13000.00
                });
                db.SaveChanges();
            }
        }




    }
}
