﻿INSERT INTO MARCA VALUES (1, 'VW - Volkswagen');
insert into marca values (2, 'Fiat');
insert into marca values (3, 'Ford');
insert into marca values (4, 'GM - Chevrolet' );

insert into Modelo values (1, 1, 'Gol' );
insert into modelo values (2, 1, 'Golf' );
insert into modelo values (3, 2, 'Uno' );
insert into modelo values (4, 2, '147' );
insert into modelo values (5, 3, 'Fiesta' );
insert into modelo values (6, 3, 'Ranger' );
insert into modelo values (7, 4, 'Cruze' );
insert into modelo values (8, 4, 'Camaro' );

insert into itemfipe values (1, 1, 1998, 7182.00);
insert into itemfipe values (2, 1, 1999, 7719.00);
insert into itemfipe values (3, 1, 2000, 8543.00);
insert into itemfipe values (4, 2, 1999, 16729.00);
insert into itemfipe values (5, 2, 2000, 18347.00);
insert into itemfipe values (6, 2, 2001, 21628.00);