﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APS_DESKTOP
{
    public partial class FormPrincipal : Form
    {
        public FormPrincipal()
        {
            InitializeComponent();
            DBInitializer.initialize();
        }

        private void imgButton_Click(object sender, EventArgs e)
        {
            new FormWizard().Show();
        }
    }
}
