﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using APS_CORE;

namespace APS_DESKTOP
{
    public enum EState
    {
        DADOS_VEICULO,
        DADOS_CONTRATO,
        RESUMO,
        DADOS_CLIENTE,
        FINALIZAR
    }

    public partial class FormWizard : Form
    {
        AppDBEntities db = new AppDBEntities();
        EState state;
        Apolice apolice;
        public FormWizard()
        {
            InitializeComponent();
            setState(EState.DADOS_VEICULO);
            cbbMarca.DataSource = db.Marca.ToList();
            cbbMarca.DisplayMember = "marca1";
            cbbMarca.ValueMember = "id";
            var combustivelCache = new Dictionary<string, int>();
            combustivelCache.Add("Gasolina", 1);
            combustivelCache.Add("Álcool", 2);
            combustivelCache.Add("Diesel", 3);
            combustivelCache.Add("Flex", 4);
            combustivelCache.Add("GNV", 5);
            combustivelCache.Add("Elétrico", 6);
            combustivelCache.Add("Outro", 7);
            cbbCombustivel.DataSource = new BindingSource(combustivelCache, null);
            cbbCombustivel.DisplayMember = "Key";
            cbbCombustivel.ValueMember = "Value";

            var categoriaCache = new Dictionary<string, string>();
            categoriaCache.Add("Categoria A", "A");
            categoriaCache.Add("Categoria B", "B");
            categoriaCache.Add("Categoria C", "C");
            categoriaCache.Add("Categoria D", "D");
            categoriaCache.Add("Categoria E", "E");
            cbbCategoria.DataSource = new BindingSource(categoriaCache, null);
            cbbCategoria.DisplayMember = "Key";
            cbbCategoria.ValueMember = "Value";

        }

        private void cbbMarca_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = ((ComboBox)sender);
            if (cb.SelectedIndex >= 0)
            {
                int value = ((Marca)cb.SelectedItem).id;
                var modelos = db.Modelo.Where(m => m.id_marca.Value.Equals(value)).ToList();
                cbbModelo.DataSource = modelos;
                cbbModelo.DisplayMember = "modelo1";
                cbbModelo.ValueMember = "id";
                cbbModelo.Enabled = true;
            }
        }

        private void cbbModelo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (cb.SelectedIndex >= 0)
            {
                int value = ((Modelo)cb.SelectedItem).id;
                var itensFipe = db.ItemFipe.Where(f => f.id_modelo.Value.Equals(value)).ToList();
                cbbAnoModelo.DataSource = itensFipe;
                cbbAnoModelo.DisplayMember = "ano";
                cbbAnoModelo.ValueMember = "id";
                cbbAnoModelo.Enabled = true;
            }

        }

        private void cbbAnoModelo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ComboBox cb = (ComboBox)sender;
            if (cb.SelectedIndex >= 0)
            {
                ItemFipe item = (ItemFipe)cb.SelectedItem;
                double vlr = item.valor.Value == null ? 0 : item.valor.Value;
                edtValorVeiculo.Value = (decimal)vlr;
            }
        }

        private void calculaValoresApolice()
        {
            double valorVeic = (double)edtValorVeiculo.Value;
            double valorApolice = valorVeic + (valorVeic * 0.10);
            double valorFranquia = (valorVeic * 0.06);
            double valorPremioInicial = valorApolice * 0.015;
            double valorPremio = valorPremioInicial;
            if ( cboxRoubos.Checked)
            {
                valorPremio += (valorPremioInicial * 0.12);
            }
            if (cboxVidros.Checked)
            {
                valorPremio += (valorPremioInicial * 0.01);
            }
            if (cboxAcidentes.Checked)
            {
                valorPremio += (valorPremioInicial * 0.04);
            }
            if (cboxTerceiros.Checked)
            {
                valorPremio += (valorPremioInicial * 0.05);
            }
            if (cboxFranqRed.Checked)
            {
                valorPremio += (valorPremioInicial * 0.03);
                valorFranquia = valorFranquia / 2;
            }
            edtValorApolice.Value = (decimal)valorApolice;
            edtValorFranquia.Value = (decimal)valorFranquia;
            edtValorPremio.Value = (decimal)valorPremio;
        }



        private Apolice fillApoliceWithData(int id_cliente)
        {
            Apolice apolice = new Apolice();
            apolice.id_cliente = id_cliente;
            apolice.id_itemfipe = (int)cbbAnoModelo.SelectedValue;
            apolice.anoFabricacao = (int)edtAnoFabricacao.Value;
            apolice.chassi = edtChassi.Text;
            apolice.placa = edtPlaca.Text;
            apolice.combustivel = (int)cbbCombustivel.SelectedValue;
            apolice.roubos = cboxRoubos.Checked;
            apolice.vidros = cboxVidros.Checked;
            apolice.acidentes = cboxAcidentes.Checked;
            apolice.danosTerceiros = cboxTerceiros.Checked;
            apolice.franqRed = cboxFranqRed.Checked;
            apolice.valorFipeContratacao = (double)edtValorVeiculo.Value;
            apolice.valorApolice = (double)edtValorApolice.Value;
            apolice.valorPremio = (double)edtValorPremio.Value;
            apolice.valorFranquia = (double)edtValorFranquia.Value;
            return apolice;
        }

        private int getNewIdCliente()
        {
            List<Cliente> clientes = this.db.Cliente.ToList();
            int id_cliente = 0;
            if (clientes != null && clientes.Count > 0)
            {
                id_cliente = clientes.Last().id;
            }
            id_cliente = id_cliente + 1;
            return id_cliente;
        }
        private void finalizarESalvar()
        {
            int id_cliente = getNewIdCliente();
            Cliente cliente = new Cliente()
            {
                id = id_cliente,
                nome = edtNome.Text,
                cpf = edtCpf.Text,
                rg = edtRg.Text,
                orgaoEmissor = edtOrgaoEmissor.Text,
                dataNascimento = edtDataNascimento.Value,
                cnh = edtCnh.Text,
                emissaoCnh = edtEmissaoCnh.Value,
                categoriaCng = cbbCategoria.SelectedValue.ToString(),
                telefone = edtTelefone.Text,
                celular = edtCelular.Text,
                email = edtEmail.Text,
                endereco = edtEndereco.Text,
                cep = edtCep.Text,
                cidade = edtCidade.Text,
                uf = edtUf.Text
            };
            this.db.Cliente.Add(cliente);
            Apolice ap = fillApoliceWithData(id_cliente);
            this.db.Apolice.Add( ap );
            this.db.SaveChanges();
            this.apolice = ap;

            MessageBox.Show("Dados Salvos Com Sucesso", "Sucesso",
                MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
        }

            private void setState(EState state)
        {
            this.state = state;
            tabControl1.SelectedIndex = (int)state;
            btnAnterior.Enabled = this.state != EState.DADOS_VEICULO;
            btnProximo.Visible = this.state != EState.FINALIZAR;
            btnAnterior.Visible = this.state != EState.FINALIZAR;
            if (this.state == EState.RESUMO)
            {
                calculaValoresApolice();
                btnProximo.Text = "Contratar";
            }
            else if (this.state == EState.DADOS_CLIENTE)
            {
                btnProximo.Text = "Confirmar Contratação";
                btnAnterior.Visible = false;
                btnSair.Text = "Cancelar";
            }
            else if( this.state == EState.FINALIZAR)
            {
                btnSair.Text = "Sair";
                
            }
            else
            {
                btnProximo.Text = "Próximo";
                btnSair.Text = "Sair";
            }
        }

        private void btnProximo_Click(object sender, EventArgs e)
        {
            switch (this.state)
            {
                case EState.DADOS_VEICULO:
                    setState(EState.DADOS_CONTRATO);
                    break;
                case EState.DADOS_CONTRATO:
                    setState(EState.RESUMO);
                    break;
                case EState.RESUMO:
                    setState(EState.DADOS_CLIENTE);
                    break;
                case EState.DADOS_CLIENTE:
                    finalizarESalvar();
                    //TODO - Salvar Dados e Imprimir
                    setState(EState.FINALIZAR);
                    break;
                case EState.FINALIZAR:
                    break;
            }
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            switch (this.state)
            {
                case EState.DADOS_CONTRATO:
                    setState(EState.DADOS_VEICULO);
                    break;
                case EState.RESUMO:
                    setState(EState.DADOS_CONTRATO);
                    break;
                case EState.DADOS_CLIENTE:
                    setState(EState.RESUMO);
                    break;
                case EState.FINALIZAR:
                    setState(EState.DADOS_CLIENTE);
                    break;
            }
        }

        private void btnSair_Click(object sender, EventArgs e)
        {
            Close();
        }

        private string montaImpressao()
        {
            Cliente c = this.db.Cliente.Where(cl => cl.id.Equals(this.apolice.id_cliente)).ToList()[0];
            StringBuilder sb = new StringBuilder();
            sb.Append("Apólice Nro:").Append(this.apolice.id)
                .Append("\n----------------------\n")
                .Append("DADOS DO CLIENTE")
                .Append("\nNome: ").Append(c.nome)
                .Append("\nData de Nascimento:").Append(c.dataNascimento)
                .Append("\nCPF: ").Append(c.cpf)
                .Append("\nEndereço:").Append(c.endereco).Append(" - ").Append(c.cep).Append(" - ").Append(c.cidade)
                        .Append(" - ").Append(c.uf)
                .Append("\nTelefone:").Append(c.telefone)
                .Append("\n----------------------\n")
                .Append("DADOS DO CONTRATO")
                .Append("\nVidros:").Append(apolice.vidros ? "Sim" : "Não");
            return sb.ToString();
               
        }

        private void button2_Click(object sender, EventArgs e)
        {
            FormImpressao imp = new FormImpressao();
            imp.setTexto(montaImpressao());
            imp.Show();
        }
    }
}
