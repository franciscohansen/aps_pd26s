﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace APS_DESKTOP.report
{
    public class ReportData
    {
        public int id { get; set; }
        public string marca { get; set; }
        public string modelo { get; set; }
        public int anoFabricacao { get; set; }
        public int combustivel { get; set; }
        public string chassi { get; set; }
        public string placa { get; set; }
        public bool roubos { get; set; }
        public bool vidros { get; set; }
        public bool acidentes { get; set; }
        public bool danosTerceiros { get; set; }
        public bool franqRed { get; set; }
        public double valorFipeContratacao { get; set; }
        public double valorFranquia { get; set; }
        public double valorApolice { get; set; }
        public double valorPremio { get; set; }
        public string nome { get; set; }

        public string endereco { get; set; }
        public string cep { get; set; }
        public string cidade { get; set; }
        public string uf { get; set; }
        public string telefone { get; set; }
        public string celular { get; set; }
        public string cpf { get; set; }
        public string rg { get; set; }
        public string orgaoEmissor { get; set; }
        public string cnh { get; set; }
        public System.DateTime emissaoCnh { get; set; }
        public string categoriaCng { get; set; }
        public System.DateTime dataNascimento { get; set; }
        public string email { get; set; }
    }
}
