﻿namespace APS_DESKTOP
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.imgButton = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.imgButton)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Location = new System.Drawing.Point(0, 312);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(651, 128);
            this.label1.TabIndex = 1;
            this.label1.Text = "Clique na Imagem para iniciar a simulação";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // imgButton
            // 
            this.imgButton.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imgButton.Image = global::APS_DESKTOP.Properties.Resources.imgButton;
            this.imgButton.Location = new System.Drawing.Point(0, 0);
            this.imgButton.Name = "imgButton";
            this.imgButton.Size = new System.Drawing.Size(651, 440);
            this.imgButton.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.imgButton.TabIndex = 0;
            this.imgButton.TabStop = false;
            this.imgButton.Click += new System.EventHandler(this.imgButton_Click);
            // 
            // FormPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 440);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.imgButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormPrincipal";
            this.Text = "Seguradora";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.imgButton)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox imgButton;
        private System.Windows.Forms.Label label1;
    }
}

