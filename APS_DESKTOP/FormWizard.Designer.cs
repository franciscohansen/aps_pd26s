﻿namespace APS_DESKTOP
{
    partial class FormWizard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabDadosVeiculo = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cbbCombustivel = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.edtPlaca = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.edtChassi = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.gboxDadosVeiculo = new System.Windows.Forms.GroupBox();
            this.cbbAnoModelo = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.edtValorVeiculo = new System.Windows.Forms.NumericUpDown();
            this.edtAnoFabricacao = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cbbModelo = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cbbMarca = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tabDadosContrato = new System.Windows.Forms.TabPage();
            this.gboxDadosContrato = new System.Windows.Forms.GroupBox();
            this.cboxFranqRed = new System.Windows.Forms.CheckBox();
            this.cboxTerceiros = new System.Windows.Forms.CheckBox();
            this.cboxAcidentes = new System.Windows.Forms.CheckBox();
            this.cboxVidros = new System.Windows.Forms.CheckBox();
            this.cboxRoubos = new System.Windows.Forms.CheckBox();
            this.tabResumo = new System.Windows.Forms.TabPage();
            this.gboxResumo = new System.Windows.Forms.GroupBox();
            this.edtValorFranquia = new System.Windows.Forms.NumericUpDown();
            this.edtValorPremio = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.edtValorApolice = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.tabDadosCliente = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label26 = new System.Windows.Forms.Label();
            this.edtUf = new System.Windows.Forms.TextBox();
            this.edtCidade = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.edtCep = new System.Windows.Forms.MaskedTextBox();
            this.edtEndereco = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.edtEmail = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.edtCelular = new System.Windows.Forms.MaskedTextBox();
            this.edtTelefone = new System.Windows.Forms.MaskedTextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label19 = new System.Windows.Forms.Label();
            this.cbbCategoria = new System.Windows.Forms.ComboBox();
            this.label18 = new System.Windows.Forms.Label();
            this.edtEmissaoCnh = new System.Windows.Forms.DateTimePicker();
            this.edtCnh = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.edtDataNascimento = new System.Windows.Forms.DateTimePicker();
            this.edtOrgaoEmissor = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.edtRg = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.edtCpf = new System.Windows.Forms.MaskedTextBox();
            this.edtNome = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.tabFinalizar = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnProximo = new System.Windows.Forms.Button();
            this.btnAnterior = new System.Windows.Forms.Button();
            this.btnSair = new System.Windows.Forms.Button();
            this.tabControl1.SuspendLayout();
            this.tabDadosVeiculo.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gboxDadosVeiculo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtValorVeiculo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtAnoFabricacao)).BeginInit();
            this.tabDadosContrato.SuspendLayout();
            this.gboxDadosContrato.SuspendLayout();
            this.tabResumo.SuspendLayout();
            this.gboxResumo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtValorFranquia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtValorPremio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtValorApolice)).BeginInit();
            this.tabDadosCliente.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabFinalizar.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.Buttons;
            this.tabControl1.Controls.Add(this.tabDadosVeiculo);
            this.tabControl1.Controls.Add(this.tabDadosContrato);
            this.tabControl1.Controls.Add(this.tabResumo);
            this.tabControl1.Controls.Add(this.tabDadosCliente);
            this.tabControl1.Controls.Add(this.tabFinalizar);
            this.tabControl1.ItemSize = new System.Drawing.Size(0, 1);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Multiline = true;
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(521, 338);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 0;
            // 
            // tabDadosVeiculo
            // 
            this.tabDadosVeiculo.Controls.Add(this.groupBox1);
            this.tabDadosVeiculo.Controls.Add(this.gboxDadosVeiculo);
            this.tabDadosVeiculo.Location = new System.Drawing.Point(4, 5);
            this.tabDadosVeiculo.Name = "tabDadosVeiculo";
            this.tabDadosVeiculo.Padding = new System.Windows.Forms.Padding(3);
            this.tabDadosVeiculo.Size = new System.Drawing.Size(513, 329);
            this.tabDadosVeiculo.TabIndex = 0;
            this.tabDadosVeiculo.Text = "DadosVeiculo";
            this.tabDadosVeiculo.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.cbbCombustivel);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.edtPlaca);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.edtChassi);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Location = new System.Drawing.Point(3, 153);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(502, 110);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Dados Adicionais do Veículo";
            // 
            // cbbCombustivel
            // 
            this.cbbCombustivel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbCombustivel.FormattingEnabled = true;
            this.cbbCombustivel.Location = new System.Drawing.Point(192, 70);
            this.cbbCombustivel.Name = "cbbCombustivel";
            this.cbbCombustivel.Size = new System.Drawing.Size(303, 21);
            this.cbbCombustivel.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(192, 55);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(66, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "Combustível";
            // 
            // edtPlaca
            // 
            this.edtPlaca.Location = new System.Drawing.Point(6, 71);
            this.edtPlaca.Name = "edtPlaca";
            this.edtPlaca.Size = new System.Drawing.Size(180, 20);
            this.edtPlaca.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 55);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(34, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Placa";
            // 
            // edtChassi
            // 
            this.edtChassi.Location = new System.Drawing.Point(6, 32);
            this.edtChassi.Name = "edtChassi";
            this.edtChassi.Size = new System.Drawing.Size(489, 20);
            this.edtChassi.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Chassi";
            // 
            // gboxDadosVeiculo
            // 
            this.gboxDadosVeiculo.AutoSize = true;
            this.gboxDadosVeiculo.Controls.Add(this.cbbAnoModelo);
            this.gboxDadosVeiculo.Controls.Add(this.label5);
            this.gboxDadosVeiculo.Controls.Add(this.edtValorVeiculo);
            this.gboxDadosVeiculo.Controls.Add(this.edtAnoFabricacao);
            this.gboxDadosVeiculo.Controls.Add(this.label4);
            this.gboxDadosVeiculo.Controls.Add(this.label3);
            this.gboxDadosVeiculo.Controls.Add(this.cbbModelo);
            this.gboxDadosVeiculo.Controls.Add(this.label2);
            this.gboxDadosVeiculo.Controls.Add(this.cbbMarca);
            this.gboxDadosVeiculo.Controls.Add(this.label1);
            this.gboxDadosVeiculo.Location = new System.Drawing.Point(3, 6);
            this.gboxDadosVeiculo.Name = "gboxDadosVeiculo";
            this.gboxDadosVeiculo.Size = new System.Drawing.Size(501, 152);
            this.gboxDadosVeiculo.TabIndex = 0;
            this.gboxDadosVeiculo.TabStop = false;
            this.gboxDadosVeiculo.Text = "Dados do Veículo";
            // 
            // cbbAnoModelo
            // 
            this.cbbAnoModelo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbAnoModelo.Enabled = false;
            this.cbbAnoModelo.FormattingEnabled = true;
            this.cbbAnoModelo.Location = new System.Drawing.Point(6, 112);
            this.cbbAnoModelo.Name = "cbbAnoModelo";
            this.cbbAnoModelo.Size = new System.Drawing.Size(120, 21);
            this.cbbAnoModelo.TabIndex = 12;
            this.cbbAnoModelo.SelectedIndexChanged += new System.EventHandler(this.cbbAnoModelo_SelectedIndexChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(255, 96);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(86, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "Valor do Veículo";
            // 
            // edtValorVeiculo
            // 
            this.edtValorVeiculo.DecimalPlaces = 2;
            this.edtValorVeiculo.Enabled = false;
            this.edtValorVeiculo.Location = new System.Drawing.Point(258, 112);
            this.edtValorVeiculo.Maximum = new decimal(new int[] {
            1000000000,
            0,
            0,
            0});
            this.edtValorVeiculo.Name = "edtValorVeiculo";
            this.edtValorVeiculo.ReadOnly = true;
            this.edtValorVeiculo.Size = new System.Drawing.Size(120, 20);
            this.edtValorVeiculo.TabIndex = 10;
            this.edtValorVeiculo.ThousandsSeparator = true;
            // 
            // edtAnoFabricacao
            // 
            this.edtAnoFabricacao.Location = new System.Drawing.Point(132, 112);
            this.edtAnoFabricacao.Maximum = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            this.edtAnoFabricacao.Minimum = new decimal(new int[] {
            1900,
            0,
            0,
            0});
            this.edtAnoFabricacao.Name = "edtAnoFabricacao";
            this.edtAnoFabricacao.Size = new System.Drawing.Size(120, 20);
            this.edtAnoFabricacao.TabIndex = 8;
            this.edtAnoFabricacao.Value = new decimal(new int[] {
            2018,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 96);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Ano Modelo";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(129, 96);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ano de Fabricação";
            // 
            // cbbModelo
            // 
            this.cbbModelo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbModelo.Enabled = false;
            this.cbbModelo.FormattingEnabled = true;
            this.cbbModelo.Location = new System.Drawing.Point(6, 72);
            this.cbbModelo.Name = "cbbModelo";
            this.cbbModelo.Size = new System.Drawing.Size(489, 21);
            this.cbbModelo.TabIndex = 3;
            this.cbbModelo.SelectedIndexChanged += new System.EventHandler(this.cbbModelo_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Modelo";
            // 
            // cbbMarca
            // 
            this.cbbMarca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbMarca.FormattingEnabled = true;
            this.cbbMarca.Location = new System.Drawing.Point(6, 32);
            this.cbbMarca.Name = "cbbMarca";
            this.cbbMarca.Size = new System.Drawing.Size(489, 21);
            this.cbbMarca.TabIndex = 1;
            this.cbbMarca.SelectedIndexChanged += new System.EventHandler(this.cbbMarca_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Marca";
            // 
            // tabDadosContrato
            // 
            this.tabDadosContrato.Controls.Add(this.gboxDadosContrato);
            this.tabDadosContrato.Location = new System.Drawing.Point(4, 5);
            this.tabDadosContrato.Name = "tabDadosContrato";
            this.tabDadosContrato.Padding = new System.Windows.Forms.Padding(3);
            this.tabDadosContrato.Size = new System.Drawing.Size(513, 329);
            this.tabDadosContrato.TabIndex = 1;
            this.tabDadosContrato.Text = "DadosContrato";
            this.tabDadosContrato.UseVisualStyleBackColor = true;
            // 
            // gboxDadosContrato
            // 
            this.gboxDadosContrato.Controls.Add(this.cboxFranqRed);
            this.gboxDadosContrato.Controls.Add(this.cboxTerceiros);
            this.gboxDadosContrato.Controls.Add(this.cboxAcidentes);
            this.gboxDadosContrato.Controls.Add(this.cboxVidros);
            this.gboxDadosContrato.Controls.Add(this.cboxRoubos);
            this.gboxDadosContrato.Location = new System.Drawing.Point(6, 6);
            this.gboxDadosContrato.Name = "gboxDadosContrato";
            this.gboxDadosContrato.Size = new System.Drawing.Size(498, 135);
            this.gboxDadosContrato.TabIndex = 0;
            this.gboxDadosContrato.TabStop = false;
            this.gboxDadosContrato.Text = "Coberturas Desejadas";
            // 
            // cboxFranqRed
            // 
            this.cboxFranqRed.AutoSize = true;
            this.cboxFranqRed.Location = new System.Drawing.Point(6, 111);
            this.cboxFranqRed.Name = "cboxFranqRed";
            this.cboxFranqRed.Size = new System.Drawing.Size(115, 17);
            this.cboxFranqRed.TabIndex = 4;
            this.cboxFranqRed.Text = "Franquia Reduzida";
            this.cboxFranqRed.UseVisualStyleBackColor = true;
            // 
            // cboxTerceiros
            // 
            this.cboxTerceiros.AutoSize = true;
            this.cboxTerceiros.Location = new System.Drawing.Point(6, 88);
            this.cboxTerceiros.Name = "cboxTerceiros";
            this.cboxTerceiros.Size = new System.Drawing.Size(138, 17);
            this.cboxTerceiros.TabIndex = 3;
            this.cboxTerceiros.Text = "Danos Contra Terceiros";
            this.cboxTerceiros.UseVisualStyleBackColor = true;
            // 
            // cboxAcidentes
            // 
            this.cboxAcidentes.AutoSize = true;
            this.cboxAcidentes.Location = new System.Drawing.Point(6, 65);
            this.cboxAcidentes.Name = "cboxAcidentes";
            this.cboxAcidentes.Size = new System.Drawing.Size(180, 17);
            this.cboxAcidentes.TabIndex = 2;
            this.cboxAcidentes.Text = "Acidentes de Qualquer Natureza";
            this.cboxAcidentes.UseVisualStyleBackColor = true;
            // 
            // cboxVidros
            // 
            this.cboxVidros.AutoSize = true;
            this.cboxVidros.Location = new System.Drawing.Point(6, 42);
            this.cboxVidros.Name = "cboxVidros";
            this.cboxVidros.Size = new System.Drawing.Size(103, 17);
            this.cboxVidros.TabIndex = 1;
            this.cboxVidros.Text = "Vidros e Granizo";
            this.cboxVidros.UseVisualStyleBackColor = true;
            // 
            // cboxRoubos
            // 
            this.cboxRoubos.AutoSize = true;
            this.cboxRoubos.Location = new System.Drawing.Point(6, 19);
            this.cboxRoubos.Name = "cboxRoubos";
            this.cboxRoubos.Size = new System.Drawing.Size(63, 17);
            this.cboxRoubos.TabIndex = 0;
            this.cboxRoubos.Text = "Roubos";
            this.cboxRoubos.UseVisualStyleBackColor = true;
            // 
            // tabResumo
            // 
            this.tabResumo.Controls.Add(this.gboxResumo);
            this.tabResumo.Location = new System.Drawing.Point(4, 5);
            this.tabResumo.Name = "tabResumo";
            this.tabResumo.Size = new System.Drawing.Size(513, 329);
            this.tabResumo.TabIndex = 2;
            this.tabResumo.Text = "Resumo";
            this.tabResumo.UseVisualStyleBackColor = true;
            // 
            // gboxResumo
            // 
            this.gboxResumo.Controls.Add(this.edtValorFranquia);
            this.gboxResumo.Controls.Add(this.edtValorPremio);
            this.gboxResumo.Controls.Add(this.label11);
            this.gboxResumo.Controls.Add(this.label10);
            this.gboxResumo.Controls.Add(this.edtValorApolice);
            this.gboxResumo.Controls.Add(this.label9);
            this.gboxResumo.Location = new System.Drawing.Point(3, 3);
            this.gboxResumo.Name = "gboxResumo";
            this.gboxResumo.Size = new System.Drawing.Size(504, 141);
            this.gboxResumo.TabIndex = 0;
            this.gboxResumo.TabStop = false;
            this.gboxResumo.Text = "Resultado da Simulação";
            // 
            // edtValorFranquia
            // 
            this.edtValorFranquia.DecimalPlaces = 2;
            this.edtValorFranquia.Enabled = false;
            this.edtValorFranquia.Location = new System.Drawing.Point(6, 110);
            this.edtValorFranquia.Maximum = new decimal(new int[] {
            1569325056,
            23283064,
            0,
            0});
            this.edtValorFranquia.Name = "edtValorFranquia";
            this.edtValorFranquia.ReadOnly = true;
            this.edtValorFranquia.Size = new System.Drawing.Size(120, 20);
            this.edtValorFranquia.TabIndex = 5;
            this.edtValorFranquia.ThousandsSeparator = true;
            // 
            // edtValorPremio
            // 
            this.edtValorPremio.DecimalPlaces = 2;
            this.edtValorPremio.Enabled = false;
            this.edtValorPremio.Location = new System.Drawing.Point(6, 71);
            this.edtValorPremio.Maximum = new decimal(new int[] {
            1569325056,
            23283064,
            0,
            0});
            this.edtValorPremio.Name = "edtValorPremio";
            this.edtValorPremio.ReadOnly = true;
            this.edtValorPremio.Size = new System.Drawing.Size(120, 20);
            this.edtValorPremio.TabIndex = 4;
            this.edtValorPremio.ThousandsSeparator = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 94);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(90, 13);
            this.label11.TabIndex = 3;
            this.label11.Text = "Valor da Franquia";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(6, 55);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Valor do Prêmio";
            // 
            // edtValorApolice
            // 
            this.edtValorApolice.DecimalPlaces = 2;
            this.edtValorApolice.Enabled = false;
            this.edtValorApolice.Location = new System.Drawing.Point(6, 32);
            this.edtValorApolice.Maximum = new decimal(new int[] {
            1569325056,
            23283064,
            0,
            0});
            this.edtValorApolice.Name = "edtValorApolice";
            this.edtValorApolice.ReadOnly = true;
            this.edtValorApolice.Size = new System.Drawing.Size(120, 20);
            this.edtValorApolice.TabIndex = 1;
            this.edtValorApolice.ThousandsSeparator = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(84, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Valor da Apólice";
            // 
            // tabDadosCliente
            // 
            this.tabDadosCliente.Controls.Add(this.groupBox3);
            this.tabDadosCliente.Controls.Add(this.groupBox2);
            this.tabDadosCliente.Location = new System.Drawing.Point(4, 5);
            this.tabDadosCliente.Name = "tabDadosCliente";
            this.tabDadosCliente.Size = new System.Drawing.Size(513, 329);
            this.tabDadosCliente.TabIndex = 3;
            this.tabDadosCliente.Text = "DadosCliente";
            this.tabDadosCliente.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.edtUf);
            this.groupBox3.Controls.Add(this.edtCidade);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.edtCep);
            this.groupBox3.Controls.Add(this.edtEndereco);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.edtEmail);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.edtCelular);
            this.groupBox3.Controls.Add(this.edtTelefone);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Location = new System.Drawing.Point(3, 148);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(507, 161);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Contato";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(406, 94);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(21, 13);
            this.label26.TabIndex = 13;
            this.label26.Text = "UF";
            // 
            // edtUf
            // 
            this.edtUf.Location = new System.Drawing.Point(406, 110);
            this.edtUf.MaxLength = 2;
            this.edtUf.Name = "edtUf";
            this.edtUf.Size = new System.Drawing.Size(95, 20);
            this.edtUf.TabIndex = 12;
            // 
            // edtCidade
            // 
            this.edtCidade.Location = new System.Drawing.Point(6, 110);
            this.edtCidade.Name = "edtCidade";
            this.edtCidade.Size = new System.Drawing.Size(394, 20);
            this.edtCidade.TabIndex = 11;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 94);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(40, 13);
            this.label25.TabIndex = 10;
            this.label25.Text = "Cidade";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(406, 55);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(28, 13);
            this.label24.TabIndex = 9;
            this.label24.Text = "CEP";
            // 
            // edtCep
            // 
            this.edtCep.Location = new System.Drawing.Point(406, 71);
            this.edtCep.Mask = "00000-000";
            this.edtCep.Name = "edtCep";
            this.edtCep.Size = new System.Drawing.Size(95, 20);
            this.edtCep.TabIndex = 8;
            // 
            // edtEndereco
            // 
            this.edtEndereco.Location = new System.Drawing.Point(6, 71);
            this.edtEndereco.Name = "edtEndereco";
            this.edtEndereco.Size = new System.Drawing.Size(394, 20);
            this.edtEndereco.TabIndex = 7;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 55);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 13);
            this.label23.TabIndex = 6;
            this.label23.Text = "Endereço";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(258, 16);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(35, 13);
            this.label22.TabIndex = 5;
            this.label22.Text = "E-mail";
            // 
            // edtEmail
            // 
            this.edtEmail.Location = new System.Drawing.Point(258, 32);
            this.edtEmail.Name = "edtEmail";
            this.edtEmail.Size = new System.Drawing.Size(243, 20);
            this.edtEmail.TabIndex = 4;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(132, 16);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(39, 13);
            this.label21.TabIndex = 3;
            this.label21.Text = "Celular";
            // 
            // edtCelular
            // 
            this.edtCelular.Location = new System.Drawing.Point(132, 32);
            this.edtCelular.Mask = "(00) 00000-0000";
            this.edtCelular.Name = "edtCelular";
            this.edtCelular.Size = new System.Drawing.Size(120, 20);
            this.edtCelular.TabIndex = 2;
            // 
            // edtTelefone
            // 
            this.edtTelefone.Location = new System.Drawing.Point(6, 32);
            this.edtTelefone.Mask = "(00) 00000-0000";
            this.edtTelefone.Name = "edtTelefone";
            this.edtTelefone.Size = new System.Drawing.Size(120, 20);
            this.edtTelefone.TabIndex = 1;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(0, 16);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(49, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Telefone";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label19);
            this.groupBox2.Controls.Add(this.cbbCategoria);
            this.groupBox2.Controls.Add(this.label18);
            this.groupBox2.Controls.Add(this.edtEmissaoCnh);
            this.groupBox2.Controls.Add(this.edtCnh);
            this.groupBox2.Controls.Add(this.label17);
            this.groupBox2.Controls.Add(this.label16);
            this.groupBox2.Controls.Add(this.edtDataNascimento);
            this.groupBox2.Controls.Add(this.edtOrgaoEmissor);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.edtRg);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.edtCpf);
            this.groupBox2.Controls.Add(this.edtNome);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(507, 139);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Identificação";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(340, 94);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(78, 13);
            this.label19.TabIndex = 15;
            this.label19.Text = "Categoria CNH";
            // 
            // cbbCategoria
            // 
            this.cbbCategoria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbbCategoria.FormattingEnabled = true;
            this.cbbCategoria.Location = new System.Drawing.Point(340, 109);
            this.cbbCategoria.Name = "cbbCategoria";
            this.cbbCategoria.Size = new System.Drawing.Size(161, 21);
            this.cbbCategoria.TabIndex = 14;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(176, 94);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(72, 13);
            this.label18.TabIndex = 13;
            this.label18.Text = "Emissão CNH";
            // 
            // edtEmissaoCnh
            // 
            this.edtEmissaoCnh.CustomFormat = "dd/MM/yyyy";
            this.edtEmissaoCnh.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.edtEmissaoCnh.Location = new System.Drawing.Point(176, 110);
            this.edtEmissaoCnh.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.edtEmissaoCnh.Name = "edtEmissaoCnh";
            this.edtEmissaoCnh.Size = new System.Drawing.Size(158, 20);
            this.edtEmissaoCnh.TabIndex = 12;
            // 
            // edtCnh
            // 
            this.edtCnh.Location = new System.Drawing.Point(6, 110);
            this.edtCnh.Name = "edtCnh";
            this.edtCnh.Size = new System.Drawing.Size(164, 20);
            this.edtCnh.TabIndex = 11;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 94);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(30, 13);
            this.label17.TabIndex = 10;
            this.label17.Text = "CNH";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(340, 55);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(104, 13);
            this.label16.TabIndex = 9;
            this.label16.Text = "Data de Nascimento";
            // 
            // edtDataNascimento
            // 
            this.edtDataNascimento.CustomFormat = "dd/MM/yyyy";
            this.edtDataNascimento.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.edtDataNascimento.Location = new System.Drawing.Point(340, 71);
            this.edtDataNascimento.MinDate = new System.DateTime(1900, 1, 1, 0, 0, 0, 0);
            this.edtDataNascimento.Name = "edtDataNascimento";
            this.edtDataNascimento.Size = new System.Drawing.Size(161, 20);
            this.edtDataNascimento.TabIndex = 8;
            this.edtDataNascimento.Value = new System.DateTime(2018, 7, 4, 22, 32, 44, 0);
            // 
            // edtOrgaoEmissor
            // 
            this.edtOrgaoEmissor.Location = new System.Drawing.Point(176, 71);
            this.edtOrgaoEmissor.Name = "edtOrgaoEmissor";
            this.edtOrgaoEmissor.Size = new System.Drawing.Size(158, 20);
            this.edtOrgaoEmissor.TabIndex = 7;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(176, 55);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 13);
            this.label15.TabIndex = 6;
            this.label15.Text = "Órgão Emissor";
            // 
            // edtRg
            // 
            this.edtRg.Location = new System.Drawing.Point(6, 71);
            this.edtRg.Name = "edtRg";
            this.edtRg.Size = new System.Drawing.Size(164, 20);
            this.edtRg.TabIndex = 5;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 55);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(23, 13);
            this.label14.TabIndex = 4;
            this.label14.Text = "RG";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(403, 16);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 13);
            this.label13.TabIndex = 3;
            this.label13.Text = "CPF";
            // 
            // edtCpf
            // 
            this.edtCpf.Location = new System.Drawing.Point(406, 32);
            this.edtCpf.Mask = "000.000.000-00";
            this.edtCpf.Name = "edtCpf";
            this.edtCpf.Size = new System.Drawing.Size(95, 20);
            this.edtCpf.TabIndex = 2;
            // 
            // edtNome
            // 
            this.edtNome.Location = new System.Drawing.Point(6, 32);
            this.edtNome.Name = "edtNome";
            this.edtNome.Size = new System.Drawing.Size(394, 20);
            this.edtNome.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "Nome";
            // 
            // tabFinalizar
            // 
            this.tabFinalizar.Controls.Add(this.button2);
            this.tabFinalizar.Controls.Add(this.button1);
            this.tabFinalizar.Location = new System.Drawing.Point(4, 5);
            this.tabFinalizar.Name = "tabFinalizar";
            this.tabFinalizar.Size = new System.Drawing.Size(513, 329);
            this.tabFinalizar.TabIndex = 4;
            this.tabFinalizar.Text = "Finalizar";
            this.tabFinalizar.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(3, 32);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(203, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "Imprimir";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(3, 3);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(203, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Enviar Dados por E-mail";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // btnProximo
            // 
            this.btnProximo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProximo.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnProximo.Location = new System.Drawing.Point(446, 356);
            this.btnProximo.Name = "btnProximo";
            this.btnProximo.Size = new System.Drawing.Size(87, 28);
            this.btnProximo.TabIndex = 1;
            this.btnProximo.Text = "Próximo";
            this.btnProximo.UseVisualStyleBackColor = true;
            this.btnProximo.Click += new System.EventHandler(this.btnProximo_Click);
            // 
            // btnAnterior
            // 
            this.btnAnterior.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAnterior.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnAnterior.Location = new System.Drawing.Point(353, 356);
            this.btnAnterior.Name = "btnAnterior";
            this.btnAnterior.Size = new System.Drawing.Size(87, 28);
            this.btnAnterior.TabIndex = 2;
            this.btnAnterior.Text = "Anterior";
            this.btnAnterior.UseVisualStyleBackColor = true;
            this.btnAnterior.Click += new System.EventHandler(this.btnAnterior_Click);
            // 
            // btnSair
            // 
            this.btnSair.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSair.Location = new System.Drawing.Point(12, 356);
            this.btnSair.Name = "btnSair";
            this.btnSair.Size = new System.Drawing.Size(87, 28);
            this.btnSair.TabIndex = 3;
            this.btnSair.Text = "Sair";
            this.btnSair.UseVisualStyleBackColor = true;
            this.btnSair.Click += new System.EventHandler(this.btnSair_Click);
            // 
            // FormWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(545, 396);
            this.Controls.Add(this.btnSair);
            this.Controls.Add(this.btnAnterior);
            this.Controls.Add(this.btnProximo);
            this.Controls.Add(this.tabControl1);
            this.Name = "FormWizard";
            this.Text = "FormWizard";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.tabControl1.ResumeLayout(false);
            this.tabDadosVeiculo.ResumeLayout(false);
            this.tabDadosVeiculo.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gboxDadosVeiculo.ResumeLayout(false);
            this.gboxDadosVeiculo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtValorVeiculo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtAnoFabricacao)).EndInit();
            this.tabDadosContrato.ResumeLayout(false);
            this.gboxDadosContrato.ResumeLayout(false);
            this.gboxDadosContrato.PerformLayout();
            this.tabResumo.ResumeLayout(false);
            this.gboxResumo.ResumeLayout(false);
            this.gboxResumo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.edtValorFranquia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtValorPremio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.edtValorApolice)).EndInit();
            this.tabDadosCliente.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabFinalizar.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabDadosVeiculo;
        private System.Windows.Forms.TabPage tabDadosContrato;
        private System.Windows.Forms.Button btnProximo;
        private System.Windows.Forms.Button btnAnterior;
        private System.Windows.Forms.Button btnSair;
        private System.Windows.Forms.GroupBox gboxDadosVeiculo;
        private System.Windows.Forms.ComboBox cbbMarca;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbbModelo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown edtAnoFabricacao;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown edtValorVeiculo;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbbCombustivel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox edtPlaca;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox edtChassi;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox gboxDadosContrato;
        private System.Windows.Forms.CheckBox cboxFranqRed;
        private System.Windows.Forms.CheckBox cboxTerceiros;
        private System.Windows.Forms.CheckBox cboxAcidentes;
        private System.Windows.Forms.CheckBox cboxVidros;
        private System.Windows.Forms.CheckBox cboxRoubos;
        private System.Windows.Forms.TabPage tabResumo;
        private System.Windows.Forms.TabPage tabDadosCliente;
        private System.Windows.Forms.TabPage tabFinalizar;
        private System.Windows.Forms.GroupBox gboxResumo;
        private System.Windows.Forms.NumericUpDown edtValorApolice;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown edtValorFranquia;
        private System.Windows.Forms.NumericUpDown edtValorPremio;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cbbAnoModelo;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.MaskedTextBox edtTelefone;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.ComboBox cbbCategoria;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.DateTimePicker edtEmissaoCnh;
        private System.Windows.Forms.TextBox edtCnh;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DateTimePicker edtDataNascimento;
        private System.Windows.Forms.TextBox edtOrgaoEmissor;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox edtRg;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.MaskedTextBox edtCpf;
        private System.Windows.Forms.TextBox edtNome;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.MaskedTextBox edtCelular;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox edtUf;
        private System.Windows.Forms.TextBox edtCidade;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.MaskedTextBox edtCep;
        private System.Windows.Forms.TextBox edtEndereco;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox edtEmail;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
    }
}