//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace APS_CORE
{
    using System;
    using System.Collections.Generic;
    
    public partial class ItemFipe
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ItemFipe()
        {
            this.Apolice = new HashSet<Apolice>();
        }
    
        public int id { get; set; }
        public Nullable<int> id_modelo { get; set; }
        public Nullable<int> ano { get; set; }
        public Nullable<double> valor { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Apolice> Apolice { get; set; }
        public virtual Modelo Modelo { get; set; }
    }
}
