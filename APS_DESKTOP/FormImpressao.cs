﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace APS_DESKTOP
{
    public partial class FormImpressao : Form
    {
        private String texto;
        public FormImpressao()
        {
            InitializeComponent();
        }

        public void setTexto(String texto)
        {
            this.texto = texto;
            this.richTextBox1.Text = texto;
        }
    }
}
